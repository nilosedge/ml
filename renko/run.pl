#!/usr/bin/perl -w

while(<>) {
	chomp;
	$_ =~ /Up: (\d+) Down: (\d+)/;
	$up = 1 * $1;
	$dn = 1 * $2;
	print ("-1\n" x $up);
	print ("1\n" x $dn);
}
