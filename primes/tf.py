#!/usr/bin/python

import tensorflow as tf

x = tf.placeholder(tf.float32, shape=[None, 100])

w1 = tf.Variable(tf.random_normal([100, 1000]))
b1 = tf.Variable(tf.random_normal([1000]))

w2 = tf.Variable(tf.random_normal([1000, 1000]))
b2 = tf.Variable(tf.random_normal([1000]))

w3 = tf.Variable(tf.random_normal([1000, 1000]))
b3 = tf.Variable(tf.random_normal([1000]))

wo = tf.Variable(tf.random_normal([1000, 100]))
bo = tf.Variable(tf.random_normal([100]))

l1 = tf.nn.relu(tf.matmul(x,  w1) + b1)
l2 = tf.nn.relu(tf.matmul(l1, w2) + b2)
l3 = tf.nn.relu(tf.matmul(l2, w3) + b3)
y = tf.nn.relu(tf.matmul(l3, wo) + bo)


