#!/usr/bin/python

with open('prime_list', 'r') as content_file:
	content = content_file.readlines()

content = [x.strip() for x in content]


lines = []
for line in content:
	lines.append(line)

i = 0
while i < len(lines) - 1:
	j = i + 1
	while j < len(lines):
		if(len(lines[j]) == 50 and len(lines[i]) == 50):
			result = str(int(lines[j]) * int(lines[i]))
			if len(result) == 100:
				#print str(j) + " " + lines[j] + " * " + str(i) + " " + lines[i] + " = " + result
				print lines[j] + "," + lines[i] + "," + result
		j += 1
	i += 1
