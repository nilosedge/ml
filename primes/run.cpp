#include <stdio.h>
#include <stdlib.h>
#include <gmp.h>

int main(int argc, char *argv[]) {
  int d = atoi(argv[1]);
  int i = 0;
  mpz_t n;
  mpz_init(n);
  mpz_ui_pow_ui(n, 10, d);
  mpz_sub_ui(n, n, 1);
  while (i < 100) {
    if (mpz_probab_prime_p(n, 100)) {
      printf("%s\n", mpz_get_str(NULL, 10, n));
      i += 1;
    }
    mpz_sub_ui(n, n, 2);
  }
}
